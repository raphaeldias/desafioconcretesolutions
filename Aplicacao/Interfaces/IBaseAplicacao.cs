﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacao.Interfaces
{
    public interface IBaseAplicacao<T>
    {
        void Inserir(T obj);
        void Atualizar(T obj);
        T ObterPorId(Int32 id);
    }
}
