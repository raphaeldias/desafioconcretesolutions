[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Apresentacao.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Apresentacao.App_Start.NinjectWebCommon), "Stop")]

namespace Apresentacao.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using System.Web.Http;
    using Aplicacao.Interfaces;
    using Aplicacao;
    using Dominio.Interfaces.Dominio;
    using Dominio;
    using Dominio.Interfaces.Repositorio;
    using Infra.Repositorios;
    using Dominio.Interfaces.Token;
    using Infra.Token;
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                GlobalConfiguration.Configuration.DependencyResolver = kernel.Get<System.Web.Http.Dependencies.IDependencyResolver>();
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IBaseAplicacao<>)).To(typeof(BaseAplicacao<>));
            kernel.Bind<IUsuarioAplicacao>().To<UsuarioAplicacao>();

            kernel.Bind(typeof(IBaseDominio<>)).To(typeof(BaseDominio<>));
            kernel.Bind<IUsuarioDominio>().To<UsuarioDominio>();

            kernel.Bind(typeof(IBaseRepositorio<>)).To(typeof(BaseRepositorio<>));
            kernel.Bind<IUsuarioRepositorio>().To<UsuarioRepositorio>();
            kernel.Bind<ITelefoneRepositorio>().To<TelefoneRepositorio>();

            kernel.Bind<IToken>().To<Token>();


        }        
    }
}
