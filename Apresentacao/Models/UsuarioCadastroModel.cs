﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Apresentacao.Models
{
    public class UsuarioCadastroModel
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public Collection<TelefoneModel> Telefones { get; set; }
    }
}