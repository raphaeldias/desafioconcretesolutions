﻿using Dominio.Interfaces.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Token
{
    public class Token : IToken
    {
        public string ObterNovo()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }
    }
}
