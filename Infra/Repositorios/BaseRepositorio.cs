﻿using Dominio.Interfaces.Repositorio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repositorios
{
    public class BaseRepositorio<T> : IBaseRepositorio<T>
    {
        public void Atualizar(T obj)
        {
            using (var session = NHibernateHelper.Instancia)
            {
                session.Update(obj);
            }
        }

        public void Inserir(T obj)
        {
            using (var session = NHibernateHelper.Instancia)
            {
                session.Save(obj);
            }
        }

        public T ObterPorId(int id)
        {
            using (var session = NHibernateHelper.Instancia)
            {
                return session.Get<T>(id);
            }
        }
    }
}
