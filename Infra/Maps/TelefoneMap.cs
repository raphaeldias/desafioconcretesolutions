﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Maps
{
    public class TelefoneMap : ClassMap<Telefone>
    {
        public TelefoneMap()
        {
            Table("telefone");

            Id(x => x.Id);
            Map(x => x.Numero);
            Map(x => x.DDD);
            References(x => x.Usuario);
        }
    }
}
