﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class TokenIncompativelException : TokenException
    {
        public TokenIncompativelException(string mensagem) : base(mensagem)
        {

        }
    }
}
