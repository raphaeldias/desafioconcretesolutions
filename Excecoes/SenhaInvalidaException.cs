﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excecoes
{
    public class SenhaInvalidaException : UsuarioException
    {
        public SenhaInvalidaException(string mensagem) : base(mensagem)
        {
        }
    }
}
