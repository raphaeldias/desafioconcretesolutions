﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Interfaces.Dominio
{
    public interface IBaseDominio<T>
    {
        void Inserir(T obj);
        void Atualizar(T obj);
        T ObterPorId(Int32 id);
    }
}
